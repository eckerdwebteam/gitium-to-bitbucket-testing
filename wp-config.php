<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-gitium-testing');

/** MySQL database username */
define('DB_USER', 'wp-gitium-test');

/** MySQL database password */
define('DB_PASSWORD', 'l3tm3s33');

/** MySQL hostname */
define('DB_HOST', 'webdb2.eckerd.edu');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2<RS1(9Vg-gxK)fU(U4jlJQ m)/rB/sAFe9!!j1[.A;]1+uINaL:x?~Vx.nGJ2p^');
define('SECURE_AUTH_KEY',  'lh{hO<#:{(s&d-G^i?3E,`knN&V|ZWRX~F]Xo@An,#oA1%yh)|FH`Uz?}j~H|Joy');
define('LOGGED_IN_KEY',    ']iAShtoS!M!Uq#a w=}nC_i#^_@*j@if#?Vv?7#&SXR2w&iLdjzVv^UO`$/2l+_Z');
define('NONCE_KEY',        'Zj4Ibi`5UlciiGg,8(=w0uCSDUy-ppCmW:)-~{^ +%h_Eh#q4SW2$3H5}PQ!I|aU');
define('AUTH_SALT',        '{JCGy>vOve`.C{^`S~KGl>A~%h9gB!,@P]M)Y!I;t^mdXdicpQB<zQX-7E#zJD+/');
define('SECURE_AUTH_SALT', 'KQQP>0TiQJo|F+&!spbrCztXiC8JoDo}E8]~h?Bym:ZF |XN:>=]u=5zpV`7>-[9');
define('LOGGED_IN_SALT',   'qRmsgp&H>7;-,J%w@K~}=z;NtS+!j--[~lJ8u@29i.+)s|`n g#h^Jh._x14#H@k');
define('NONCE_SALT',       'v}d+0qJXp(#O;zZKk=#~ 94twDELq5FJ):-McW0XFU-LF#s~<2L*pQrHV7}kK-A+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
